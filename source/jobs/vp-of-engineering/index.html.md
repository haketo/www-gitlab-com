---
layout: markdown_page
title: "Vice President of Engineering"
---

## Responsibilities

* Support CEO in execution of company strategy.
* Work with CTO and product leads to execute technical vision for GitLab.
* Work across all of engineering teams to deliver a quality product.
* Signal potential problems to CEO.
* Tackle cross-disciplinary / cross-team initiatives.
* Be available for quick turnaround time on new tasks.
* Lead software development by example.
* Ensure [software quality](https://en.wikipedia.org/wiki/Software_quality).
* Ensure GitLab releases a new version every 22nd of the month.

### Current specific responsibilities

* Prioritize work and resources across Frontend, Infrastructure, Support, and Development teams.
* Assist with milestone planning and work estimation.
* Lead performance reviews and hiring.
* Improve tooling and processes to minimize downtime and improve software performance.
* Help answer technical questions and issues.
* Mention to get stuff in the handbook and issues; don't leave them in Docs or chat.
