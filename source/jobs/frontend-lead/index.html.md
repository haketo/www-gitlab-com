---
layout: markdown_page
title: "Frontend Lead"
---

The Frontend Lead reports to the CTO, and Frontend engineers report to them.

## Responsibilities

* Interviews applicants for Frontend engineers positions
* Continues to spend part of their time coding
* Ensures that the technical decisions and process set by the CTO are followed
* Does 1:1's with all reports every 2-5 weeks (depending on the experience of the report)
* Is available for 1:1's on demand of the report
* Uses the contributor analytics to ensure that Frontend engineers that are stuck are helped
* Ensures quality implementation of design materials
* Prioritize frontend issues which lead to bad user experience
* Review of the merge requests made by Frontend engineers
* Delivers input on promotions, function changes, demotions and firings in consultation with the CEO and CTO